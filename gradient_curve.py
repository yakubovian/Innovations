# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 15:34:20 2016

@author: Clark
"""

import numpy as np
import matplotlib.pyplot as plt


def Level2(source):
    c = source
    mat = matrix[0]
    summ = 0
    for k in [c[0]-1, c[0], c[0]+1]:
        for l in [c[1]-1, c[1], c[1]+1]:
            try:
                if k < 0 or l < 0:
                    raise IndexError
                else:
                    summ += mat[k][l]['innovator'] 
                        
            except IndexError: pass
    summ -= mat[c[0]][c[1]]['innovator']
    return summ             
    
    
def ploter(gradi):    
  
    fig = plt.figure()
    ax = fig.add_subplot(111)        
          
    for i in range(len(gradi)):
        ax.scatter(i+1, gradi[i])
        
    plt.xlabel('Time step')
    plt.ylabel('Gradient')
    st = 'Threshold %.2f' % threshold
    plt.title(st)
    plt.show()
    
def plot(flagi, gradi):
    
    derivative = [ flagi[i+1] - flagi[i] for i in range(len(flagi)-1) ]
    
    if sum(derivative) == 0:
        return 0
    else:
    
        plt.subplot(3, 1, 1)
        for i in range(len(flagi)):
            plt.scatter(i+1, flagi[i])    
        plt.title('Diffusion of Innovation')
        plt.ylabel('Number of actives')
        plt.grid(True)        
        
        plt.subplot(3, 1, 3)
        for i in range(len(flagi)):
            plt.scatter(i+1, gradi[i])
        plt.xlabel('Time (step)')
        plt.ylabel('Phase')
        plt.grid(True)
        
        plt.subplot(3, 1, 2)
        for i in range(len(flagi)-1):
            plt.scatter(i+1, derivative[i]) 
        plt.ylabel('Velocity ')
        plt.grid(True)
        
        plt.show()

    

nx, ny, nt = 8, 10, 25
threshold = 0.3
num = nx*ny

    # open file with region data and adding initial conditions

f = open('S:\Docs\Diffusion of innovation\innovation_clasters.txt')
regions = {}
for line in iter(f):
    ls = line.split('\t')
    name, innovations, people, claster =  ls[1], float(ls[2]), float(ls[3]) * 1000., float(ls[6])
    pos2, pos1 = float(ls[4]), float(ls[5][:-1]) 
    index = claster/5
    regions[name] = [index,pos2,pos1]

positions = sorted([  [ regions[key][2], regions[key][1], key ] for key in regions.keys()  ])

#lev = []    # level sum number
  

matrix = np.zeros((nt,nx,ny),dtype=object)

for t in range(nt):
    for x in range(nx):
        matrix[t][x][:] = positions[x*ny:(x+1)*ny]  
        for y in range(ny):
            reg = matrix[t][x][y]
            index = regions[reg[2]][0]
            el = dict(( ('name',reg[2]), ('innovator',index), ('state',0)  ))
            matrix[t][x][y] = el

#actives = np.zeros((nx,ny))     # for each source

gradient = []
reg_flags = []

for xx in range(1):
    for yy in range(1): 
        for t in range(nt):        
            matrix[t][7][5]['state'] = 1     #    source
            source = (yy,xx)
   
        t = 1
        flag = 0
        reg_grad = []
        flags = []

        while (flag != num and t < nt-1):   #   logic cicle

            for i in range(nx): 
                for j in range(ny):                         #   elementos cicles
                    summ = 0
                    for pair in ([i-1,j], [i,j-1], [i,j+1], [i+1,j]):
                        try:
                            if pair[0] < 0 or pair[1] < 0:
                                raise IndexError
                            else:
                                summ += matrix[t-1][pair[0]][pair[1]]['state'] 
                                   
                        except IndexError:
                            summ += 0
                                
                    summ -= matrix[t-1][i][j]['state'] 
                    fate = summ * matrix[t-1][i][j]['innovator']
                    matrix[t][i][j]['state'] = matrix[t-1][i][j]['state']
                    if fate >= threshold: 
                        matrix[t][i][j]['state'] = 1
                        
            
            
            grad = []
            for i in range(nx): 
                for j in range(ny):                         #   elementos cicles
                    for pair in ([i-1,j], [i,j-1], [i,j+1], [i+1,j]):
                        try:
                            if pair[0] < 0 or pair[1] < 0:
                                raise IndexError
                            else:
                                dif = abs(matrix[t][i][j]['state'] - matrix[t][pair[0]][pair[1]]['state'])
                                if dif > 0:
                                    grad.append(0.5)
                                       
                        except IndexError:
                            pass
                                       
            
            
            flag = 0                
            for i in range(nx):
                for j in range(ny):
                    flag += matrix[t][i][j]['state']  
                    
            flags.append(flag)
        
            t += 1

            reg_grad.append(sum(grad))
       
        #actives[yy,xx] = flag
        #lev.append([Level2(source), flag])
            
        gradient.append(reg_grad)
        reg_flags.append(flags)
        
#        for i in range(nx):
#            for j in range(ny):
#                for t in range(nt):
#                    matrix[t][i][j]['state'] = 0
                    
                    
for i in range(len(gradient)):
    
    plot(reg_flags[i], gradient[i])
#

#for i in gradient:
#    ploter(i)
    
#fig = plt.figure()
#ax = fig.add_subplot(111)        
#      
#for i in range(num):
#    ax.scatter(lev[i][0], lev[i][1])
#
#plt.xlabel('Summ index of level')
#plt.ylabel('Number of adoptions')
#st = 'Threshold %.2f' % threshold
#plt.title(st)

                        




