# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 09:30:15 2017

@author: Clark
"""

import numpy as np
import matplotlib.pyplot as plt

def initialization():
        # open file with region data and adding initial conditions

    f = open('S:\\Docs\\Diffusion of innovation\\New_cellular_automatous\\clasters2.txt')
    regions = {}
#    indexes = { 5:5, 2:4, 3:3, 1:2, 4:1 }
#    indexes = { 5:5, 2:3, 3:4, 1:2, 4:1 }
    indexes = { 5:5, 2:4, 3:3, 1:2, 4:1 }
    peoples = {}
    squares = {}
    for line in iter(f):
        ls = line.split('\t')
        name, innovations, people, claster =  ls[1], float(ls[2]), float(ls[3]) * 1000., float(ls[8])
        pos2, pos1 = float(ls[4]), float(ls[5][:-1]) 
        square, percent = float(ls[6]), float(ls[7].replace(',','.')) 
        index = indexes[claster]
        regions[name] = [index,pos2,pos1]
        peoples[name] = people
        squares[name] = [square, percent]
    positions = sorted([  [ regions[key][2], regions[key][1], key ] for key in regions.keys()  ])
    f.close()
    
    matrix = np.zeros((nx,ny),dtype=object)
    for y in range(ny):
        pos = positions[y*nx:(y+1)*nx]
        matrix[:,y] = sorted([ [reg[1],reg[0],reg[2]] for reg in pos ],reverse=True)
        for x in range(nx):
            reg = matrix[x][y]
            index = regions[reg[2]][0]
            el = { 'name':reg[2], 'innovator':index, 'state':0 }
            neibs = []
            for l in range(x-1, x+2):
                for k in range(y-1, y+2):
                    if (l in [-1,nx]) or (k in [-1,ny]):
                        pass
                    else:
                        neibs.append([l,k])
            neibs.remove([x,y])
            areal[x,y] = {  'index' : [ el['innovator'] ] * (nt+1),
                            'neibs': neibs, 'people':peoples[reg[2]], 'name':reg[2],
                            'area': squares[reg[2]],
                            'fails': 0}

def automatous():
    t = 0
    while (t < nt) :
        for i in range(nx):
            for j in range(ny):
                cell = areal[i,j]
                trigger = 0
                for neib in cell['neibs']:
                    neibour = areal[neib[0],neib[1]]
                    if abs(neibour['index'][t] - cell['index'][t]) == difference_between_classes:
                        trigger += 1
                if trigger >= threshold_up[cell['index'][t]]:
                    cell['index'][t+1] = min(5, cell['index'][t] + 1)
                else:
                    if cell['fails'] >= threshold_down[cell['index'][t]]:
                        cell['index'][t+1] = max(1, cell['index'][t] - 1)
                        cell['fails'] = 0
                    else:
                        cell['index'][t+1] = cell['index'][t] 
                        cell['fails'] += 1
        t += 1                   

nx, ny, nt = 7, 11,500
areal = np.zeros((nx,ny),dtype=dict)

#одинаковые условия
#threshold_up = {5:3, 4:3, 3:3, 2:3, 1:3}
#threshold_down = {5:3, 4:3, 3:3, 2:3, 1:3}           

#роджерс
#threshold_up = {5:4, 4:5, 3:4, 2:3, 1:2}
#threshold_down = {5:1, 4:1, 3:3, 2:5, 1:5}

#стационарное
#threshold_up = {5:5, 4:4, 3:5, 2:3, 1:5}
#threshold_down = {5:2, 4:4, 3:2, 2:2, 1:1}

#прогрессивный 
threshold_up = {5:4, 4:4, 3:3, 2:2, 1:1}
threshold_down = {5:1, 4:2, 3:3, 2:4, 1:5}

difference_between_classes = 1

classes = [ 'innov',         #   5
            'early',         #   4
            'major',         #   3
            'late',          #   2
            'lag' ]          #   1

              
initialization()
automatous()

hist = []
for i in range(nx):
    for j in range(ny):
        index =  areal[i,j]['index'][-1]
        hist.append(index)
plt.hist(hist)
plt.ylabel('Number of regions')
plt.xlabel('Index')
plt.show()

pep = np.zeros(5)
for i in range(nx):
    for j in range(ny):
        man = areal[i,j]['people'] * 1e-4
        ind = areal[i,j]['index'][-1]
        for k in range(1,6):
            if ind == k:
                pep[k-1] += man
                break
pep = [ int(i*1e-2) for i in pep ]
out = []
for i in range(1,6):
    out = out + [i] * pep[i-1]
plt.hist(out,color='green')
plt.ylabel('Million people')
plt.xlabel('Index')
plt.show()

#
#sqr = np.zeros(5)
#for i in range(nx):
#    for j in range(ny):
#        s = areal[i,j]['area'][1] 
#        ind = areal[i,j]['index'][0]
#        for k in range(1,6):
#            if ind == k:
#                sqr[k-1] += s
#                break
#out = []
#for i in range(1,6):
#    out = out + [i] * int(sqr[i-1])
#plt.hist(out,color='red')
#plt.ylabel('Area %')
#plt.xlabel('Index')
#plt.show()

im = np.zeros((nx,ny))
for i in range(nx):
    for j in range(ny):
        im[i,j] = areal[i,j]['index'][-1]
plt.imshow(im,interpolation='nearest')
plt.show()

