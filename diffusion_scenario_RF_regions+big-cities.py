# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 09:30:15 2017

@author: Clark
"""

import numpy as np
import matplotlib.pyplot as plt

def initialization():
        # open file with region data and adding initial conditions

    f = open('S:\\Docs\\Diffusion of innovation\\Code\\clasters.txt')
    regions = {}
    indexes = { 5:5, 2:4, 3:3, 1:2, 4:1 }
#    indexes = { 5:5, 2:1, 3:4, 1:3, 4:2 }
    
    for line in iter(f):
        ls = line.split('\t')
        name, innovations, people, claster =  ls[1], float(ls[2]), float(ls[3]) * 1000., float(ls[6])
        pos2, pos1 = float(ls[4]), float(ls[5][:-1]) 
        index = indexes[claster]
        regions[name] = [index,pos2,pos1]
    
    positions = sorted([  [ regions[key][2], regions[key][1], key ] for key in regions.keys()  ])
    f.close()
    
    matrix = np.zeros((nx,ny),dtype=dict)
    for y in range(ny):
        pos = positions[y*nx:(y+1)*nx]
        matrix[:,y] = sorted([ [reg[1],reg[0],reg[2]] for reg in pos ],reverse=True)
        for x in range(nx):
            reg = matrix[x][y]
            index = regions[reg[2]][0]
            el = dict(( ('name',reg[2]), ('innovator',index), ('state',0)  ))
            
            #   dimension increasing 
            mx, my = 1 + x*3, 1 + y*3
            for l in range(mx-1, mx+2):
                for k in range(my-1, my+2):
                    areal[l,k] = {  'index' : [ el['innovator'] ] * (nt+1),
                                        'fails': 0}
    for x in range(3*nx):
        for y in range(3*ny):
            neibs = []
            for l in range(x-1, x+2):
                for k in range(y-1, y+2):
                    if (l in [-1,3*nx]) or (k in [-1,3*ny]):
                        pass
                    else:             
                        neibs.append([l,k])
            neibs.remove([x,y])
            areal[x,y]['neibs'] = neibs 


def automatous():
        #   cellular automatous
    t = 0
    while (t < nt) :
        for i in range(3*nx):
            for j in range(3*ny):
                cell = areal[i,j]
                trigger = 0
                for neib in cell['neibs']:
                    neibour = areal[neib[0],neib[1]]
                    if abs(neibour['index'][t] - cell['index'][t]) == difference_between_classes:
                        trigger += 1
                if trigger >= threshold_up[cell['index'][t]]:
                    cell['index'][t+1] = min(5, cell['index'][t] + 1)
                else:
                    if cell['fails'] >= threshold_down[cell['index'][t]]:
                        cell['index'][t+1] = max(1, cell['index'][t] - 1)
                        cell['fails'] = 0
                    else:
                        cell['index'][t+1] = cell['index'][t] 
                        cell['fails'] += 1
        t += 1

nx, ny, nt = 7, 11, 100
areal = np.zeros((3*nx,3*ny),dtype=dict)
#threshold_up = {5:3, 4:3, 3:3, 2:3, 1:3}
#threshold_down = {5:2, 4:3, 3:3, 2:5, 1:4}

#threshold_up = {5:4, 4:5, 3:4, 2:3, 1:3}
#threshold_down = {5:2, 4:2, 3:2, 2:1, 1:5}

threshold_up = {5:4, 4:4, 3:5, 2:3, 1:3}
threshold_down = {5:3, 4:3, 3:3, 2:3, 1:3}

    
difference_between_classes = 1
classes = [ 'innov',         #   5
            'early',         #   4
            'major',         #   3
            'late',          #   2
            'lag' ]          #   1

initialization()
                   
       
automatous()
            
states = np.zeros((3*nx,3*ny))
hist = []
for i in range(3*nx):
    for j in range(3*ny):
        index =  areal[i,j]['index'][-1]
        states[i,j] = index
        hist.append(index)
plt.imshow(states,interpolation='nearest')
plt.show()
plt.hist(hist)
plt.show()