# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 15:34:20 2016

@author: Clark
"""

import numpy as np
import matplotlib.pyplot as plt


def automatous(tt,k,l):
    summ = 0
    for pair in ( [k-1,l], [k,l-1],[k,l+1], [k+1,l] ):
        try:
            if pair[0] < 0 or pair[1] < 0:
                raise IndexError
            else:
                summ += matrix[tt-1][pair[0]][pair[1]]['state'] 
                   
        except IndexError:
            summ += 0
    return summ

def decision(fat):
    #num = 1./(1+np.exp(-5*(fat-1.)))
    num = 1./(1+np.exp(-2*(fat-2.5)))
    choice = int(num * 100)
    choice = choice*[1] + (100-choice)*[0]
    return np.random.choice(choice)
    

def Level(source):
    c = source
    mat = matrix[0]
    summ = 0
    for k in [c[0]-1, c[0]+1]:
        for l in [c[1]-1, c[1]+1]:
            try:
                if k < 0 or l < 0:
                    raise IndexError
                else:
                    summ += mat[k][l]['innovator'] 
                        
            except IndexError: pass
    
    return summ            
    
    
def ploter(flagi, gradi, sori):
    
    derivative = [ flagi[i+1] - flagi[i] for i in range(len(flagi)-1) ]
    
    if sum(derivative) == 0:
        return 0
    else:
    
        plt.subplot(3, 1, 1)
        for i in range(len(flagi)):
            plt.scatter(i+1, flagi[i])    
        plt.title( u'The source index is %s ' % (str(sori)) )
        plt.ylabel(u'Number of actives')
        plt.grid(True)        
        
        plt.subplot(3, 1, 3)
        for i in range(len(flagi)):
            plt.scatter(i+1, gradi[i])
        plt.xlabel(u'Time (step)')
        plt.ylabel('Phase')
        plt.grid(True)
        
        plt.subplot(3, 1, 2)
        for i in range(len(flagi)-1):
            plt.scatter(i+1, derivative[i]) 
        plt.ylabel(u'Velocity ')
        plt.grid(True)
        
        plt.show()
        
def plot(flagi,name=None):
    for i in range(len(flagi)):
        plt.scatter(i+1,flagi[i])
    plt.grid(True)
    plt.xlabel('Time step')
    plt.ylabel('Number of adoptions')
    if name is None:
        plt.title('All of regions')
        plt.axis([0,30,0,77])
    else:
        plt.title(name)
        plt.axis([0,nt,0,70])
    plt.show()

def spc(espec):
    keys = espec.keys()
    plt.subplot(221)
    for j in range(len(espec[keys[0]])):
        plt.scatter(j+1,espec[keys[0]][j])
    plt.axis([0,30,0,77])
    #plt.xlabel('Time step')
    plt.ylabel('Number of adoptions')
    plt.title(keys[0])
    
    plt.subplot(222)
    for j in range(len(espec[keys[1]])):
        plt.scatter(j+1,espec[keys[1]][j])
    plt.axis([0,30,0,77])
    #plt.xlabel('Time step')
    #plt.ylabel('Number of adoptions')
    plt.title(keys[1])
    
    plt.subplot(223)
    for j in range(len(espec[keys[2]])):
        plt.scatter(j+1,espec[keys[2]][j])
    plt.axis([0,30,0,77])
    plt.xlabel('Time step')
    plt.ylabel('Number of adoptions')
    plt.title(keys[2])
    
    plt.subplot(224)
    for j in range(len(espec[keys[3]])):
        plt.scatter(j+1,espec[keys[3]][j])
    plt.axis([0,30,0,77])
    plt.xlabel('Time step')
    #plt.ylabel('Number of adoptions')
    plt.title(keys[3])
    
    plt.show()
    
    
def spy(tt):
    mat = np.zeros((nx,ny))
    for xx in range(nx):
        for yy in range(ny):
            mat[xx,yy] = matrix[tt][xx][yy]['state']
    plt.spy(mat,markersize=10)
    plt.show()
    
def imshow():
    index = np.zeros((nx,ny))
    statesf = np.zeros((nx,ny))
    statesi = np.zeros((nx,ny))
    for i in range(nx):
        for j in range(ny):
            statesf[i,j] = matrix[-1][i][j]['state']
            index[i,j] = matrix[-1][i][j]['innovator']
            statesi[i,j] = matrix[50][i][j]['state']
    statesf[source[0]][source[1]] = 1.5
    statesi[source[0]][source[1]] = 1.5
    
    plt.subplot(1,3,1)
    plt.imshow(index, interpolation='nearest')
    plt.title('Index')
    
    plt.subplot(1,3,2)
    plt.imshow(statesi, plt.cm.gray, interpolation='nearest')
    plt.title('Intermediate state')
    
    plt.subplot(1,3,3)
    plt.imshow(statesf, plt.cm.gray, interpolation='nearest')
    plt.title('Final state')
    
    
    
    
        

nx, ny, nt = 7, 11, 100
num = nx*ny

    # open file with region data and adding initial conditions

f = open('S:\\Docs\\Diffusion of innovation\\Code\\clasters.txt')
regions = {}
#indexes = { 4:1, 2:2, 3:3, 5:4, 1:5 } # actual state index from the file
#indexes = { 5:1, 4:0.15, 3:0.4, 2:0.85, 1:0.25 }
#indexes = { 5:3.2, 4:1., 3:2.1, 2:2.6, 1:1.8 }
indexes = { 5:2.5, 2:2.1, 3:1.6, 1:1.1, 4:0.5 }

for line in iter(f):
    ls = line.split('\t')
    name, innovations, people, claster =  ls[1], float(ls[2]), float(ls[3]) * 1000., float(ls[6])
    pos2, pos1 = float(ls[4]), float(ls[5][:-1]) 
    index = indexes[claster]
    regions[name] = [index,pos2,pos1]

positions = sorted([  [ regions[key][2], regions[key][1], key ] for key in regions.keys()  ])
f.close()

matrix = np.zeros((nt,nx,ny),dtype=object)


for t in range(nt):
    for y in range(ny):
        pos = positions[y*nx:(y+1)*nx]
        matrix[t,:,y] = sorted([ [reg[1],reg[0],reg[2]] for reg in pos ],reverse=True)
        for x in range(nx):
            reg = matrix[t][x][y]
            index = regions[reg[2]][0]
            el = dict(( ('name',reg[2]), ('innovator',index), ('state',0)  ))
            matrix[t][x][y] = el

#with open('S:\\distrib.txt','w') as f:
#    for i in range(nx):
#        for j in range(ny):
#            f.write('%d %d %d \n' % (matrix[0][i,j]['innovator'],i,j))


reg_flags = []
especial = {}
for xx in range(1):
    for yy in range(1): 
        for t in range(nt):    
            source = [0,4]
            matrix[t][source[0]][source[1]]['state'] = 1     #    source

   
        t = 0
        flag = 0
        flags = []

        while (flag != num and t < nt-1):   #   logic cicle
            t += 1
            for i in range(nx): 
                for j in range(ny):                         #   elementos cicles
                    summ = automatous(t,i,j)
                    fate = summ * matrix[t-1][i][j]['innovator']
                    if matrix[t-1][i][j]['state'] == 1:
                        matrix[t][i][j]['state'] = matrix[t-1][i][j]['state']
                    else:
                        matrix[t][i][j]['state'] = decision(fate)
#                    matrix[t][i][j]['state'] = decision(fate)
                    
            
            flag = 0              
            for i in range(nx):
                for j in range(ny):
                    flag += matrix[t-1][i][j]['state']  
                    
            flags.append(flag)
              
       
        reg_flags.append(flags)
        if matrix[t][xx][yy]['name'] in ['St. Petersburg','Moscow','NOVOSIBIRSK',
                                        'KOMI', 'TYUMEN','ALTAY','Tuva Republic','Irkutsk',
                                        'SAKHA','AMUR','Khabarovsk','KAMCHAT']:
            especial[matrix[t][xx][yy]['name']] = flags
                                            
        
#        for i in range(nx):
#            for j in range(ny):
#                for t in range(nt):
#                    matrix[t][i][j]['state'] = 0
                    
                    
#for i in range(len(reg_flags)):
##    ploter(reg_flags[i], gradient[i] , sources[i])
#    plot(reg_flags[i])

#spy(t)
plot(flags,'Diffusion of innovation')
#states = np.zeros((nx,ny))
#for i in range(nx):
#    for j in range(ny):
#        states[i,j] = matrix[-1][i][j]['state']
#states[source[0]][source[1]] = 1.5
#plt.imshow(states, interpolation='nearest')
imshow()




                        




