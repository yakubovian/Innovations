# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 15:34:20 2016

@author: Clark
"""

import numpy as np
import matplotlib.pyplot as plt


def automatous(tt,k,l):
    summ = 0
    for pair in ( [k-1,l], [k,l-1],[k,l+1], [k+1,l] ):
        try:
            if pair[0] < 0 or pair[1] < 0:
                raise IndexError
            else:
                summ += matrix[tt-1][pair[0]][pair[1]]['state'] 
                   
        except IndexError:
            summ += 0
    return summ


def decision(fat):
    num = 1./(1+np.exp(-2*(fat-2.5)))
    choice = int(num * 100)
    choice = choice*[1] + (100-choice)*[0]
    return np.random.choice(choice)

        
def plot(flagi,name=None):
    for i in range(len(flagi)):
        plt.scatter(i+1,flagi[i])
    plt.grid(True)
    plt.xlabel('Time step')
    plt.ylabel('Number of adoptions')
    if name is None:
        plt.title('All of regions')
        plt.axis([0,30,0,77])
    else:
        plt.title(name)
        plt.axis([0,nt,0,70])
    plt.show()

    
def spy(tt):
    mat = np.zeros((nx,ny))
    for xx in range(nx):
        for yy in range(ny):
            mat[xx,yy] = matrix[tt][xx][yy]['state']
    plt.spy(mat,markersize=10)
    plt.show()
    

def mana(mat,tt,l,m):
    innov = mat[tt][l][m]['innovator']
    state = mat[tt][l][m]['state']
    if innov == 1:
        if state == 1:  mat[tt][l][m]['mana'] = mat[tt-1][l][m]['mana'] + 1.8
        else: mat[tt][l][m]['mana'] = mat[tt-1][l][m]['mana'] - 0.06
    if innov == 2:
        if state == 1:  mat[tt][l][m]['mana'] = mat[tt-1][l][m]['mana'] + 1.8
        else: mat[tt][l][m]['mana'] = mat[tt-1][l][m]['mana'] - 0.12
    if innov == 3:
        if state == 1:  mat[tt][l][m]['mana'] = mat[tt-1][l][m]['mana'] + 1.5
        else: mat[tt][l][m]['mana'] = mat[tt-1][l][m]['mana'] - 0.06

        
def transition(reg):
    man = reg['mana']
    innov = reg['innovator']
    
    if innov == 1:
        if man >= 3.5 :  
            innov = 2
        return innov
            
    if innov == 2:
        if man >= 12:
            innov = 3
        if man  <= -1.5: 
            innov = 1
        return innov
            
    if innov == 3:
        if man  <= -3.5: 
            innov = 2
        return innov
            
    
        

nx, ny, nt = 7, 10, 20
num = nx*ny

matrix = np.zeros((nt,nx,ny),dtype=object)


for t in range(nt):
    for x in range(nx):
        for y in range(ny):
            el = dict(( ('innovator',1), ('state',0), ('mana',1)  ))
            matrix[t][x][y] = el

reg_flags = []

n = 10
for nn in range(n):
    for xx in range(nx):
        for yy in range(ny): 
            for t in range(nt):    
                source = (xx,yy)
                matrix[t][source[0]][source[1]]['state'] = 1     #    source
       
            t = 0
            flag = 0    
            while (flag != num and t < nt-1):   #   logic cicle
                t += 1
                for i in range(nx): 
                    for j in range(ny):                         #   elementos cicles
                        if matrix[t-1][i][j]['state'] == 1:
                            matrix[t][i][j]['state'] = matrix[t-1][i][j]['state']
                        else:
                            summ = automatous(t,i,j)
                            fate = summ * matrix[t-1][i][j]['innovator']
                            if summ != 0:
                                matrix[t][i][j]['state'] = decision(fate)
                                mana(matrix, t,i,j)
                        
                
                flag = 0              
                for i in range(nx):
                    for j in range(ny):
                        flag += matrix[t-1][i][j]['state']  
                             
        
            for i in range(nx):
                for j in range(ny):
                    innov = transition(matrix[t][i][j])
                    for t in range(nt):
                        if matrix[t][i][j]['innovator'] != innov:
                            matrix[t][i][j]['mana'] = 0
                        matrix[t][i][j]['innovator'] = innov
                        matrix[t][i][j]['state'] = 0
    
#    mn = np.zeros((nx,ny))
#    for i in range(nx):
#        for j in range(ny):
#            mn[i,j] = matrix[0][i][j]['innovator']
#    plt.imshow(mn,interpolation='none',cmap='binary')
#    plt.show()
#    s = "S:\\pictures\\" + str(nn) + '.png'
#    fig = plt.gcf()
#    plt.show()
#    plt.draw()
#    fig.savefig(s)
#    plt.close()

                    

#plot(flags,'Diffusion of innovation')
#spy(t)
indexes = []
for i in range(nx):
    for j in range(ny):
        indexes.append(matrix[t,i,j]['innovator'])
plt.hist(indexes,5,normed=1)
        
        
 



                        




